public class PartThree
{
	final static java.util.Scanner scan = new java.util.Scanner(System.in);
	public static void main(String[]args)
	{
		AreaComputations Ac = new AreaComputations();
		System.out.println("enter Square length");
		double Square = scan.nextDouble();
		System.out.println("enter rectangle length");
		double RectangleLength = scan.nextDouble();
		System.out.println("enter rectangle width");
		double RectangleWidth = scan.nextDouble();
		System.out.println("Square area = " + Ac.areaSquare(Square) + "\nthe area of the rectangle is " + Ac.areaRectangle(RectangleLength, RectangleWidth));
	}
}